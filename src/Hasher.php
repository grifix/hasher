<?php

declare(strict_types=1);

namespace Grifix\Hasher;

final class Hasher implements HasherInterface
{
    public function __construct(private readonly string $key, private readonly string $algorithm = 'sha256')
    {
    }

    public function hash(string $data): string
    {
        return hash_hmac($this->algorithm, $data, $this->key);
    }
}
