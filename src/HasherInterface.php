<?php

declare(strict_types=1);

namespace Grifix\Hasher;

interface HasherInterface
{
    public function hash(string $data): string;
}
