<?php

declare(strict_types=1);

namespace Grifix\Hasher\Tests;

use Grifix\Hasher\Hasher;
use PHPUnit\Framework\TestCase;

final class HasherTest extends TestCase
{
    public function testItWorks(): void
    {
        $hasher = new Hasher('key1');
        $data   = 'data';
        $hash   = $hasher->hash($data);
        self::assertEquals('03b904db4cd4199f7135a07290e329da8a42113257fcdeeaa0cea11247131f4e', $hash);
        $hasher2 = new Hasher('key2');
        self::assertNotEquals($hasher2->hash($data), $hasher->hash($data));
    }
}
